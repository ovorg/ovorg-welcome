import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

// Views
import Login from "@/views/Login";
import Presence from "@/views/Presence";
import Meeting from "@/views/Meeting";
import Calendar from "@/views/Calendar";
import Alarm from "@/views/Alarm";

Vue.use(VueRouter);

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/login");
};

const routes = [
  { path: "/", redirect: { name: "presence" } },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/login/:client_id/:token",
    name: "tokenLogin",
    component: Login
  },
  {
    path: "/anwesenheit",
    name: "presence",
    component: Presence,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/dienst",
    name: "meeting",
    component: Meeting,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/kalender",
    name: "calendar",
    component: Calendar,
    beforeEnter: ifAuthenticated
  },
  {
    path: "/einsatz",
    name: "alarm",
    component: Alarm,
    beforeEnter: ifAuthenticated
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
