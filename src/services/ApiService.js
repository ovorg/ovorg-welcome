import axios from "axios";
import store from "@/store";
import createAuthRefreshInterceptor from "@/refresh";
import handleError from "@/axiosErrorHandler";

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL
});

apiClient.defaults.headers.common["Accept"] = "application/json";
apiClient.interceptors.request.use(request => {
  if (store.getters.isAuthenticated) {
    request.headers["Authorization"] = store.getters.bearer;
  }
  return request;
});

apiClient.interceptors.response.use(res => res, handleError);

createAuthRefreshInterceptor(apiClient, failedRequest =>
  store.dispatch("authRefresh", failedRequest)
);

export default {
  login(credentials) {
    return apiClient.post("/oauth/token", credentials);
  },
  refresh(oldRefreshToken) {
    return apiClient.post("/api/refresh", { refresh_token: oldRefreshToken });
  },
  timeTracking() {
    return apiClient.get("/api/time_tracking");
  },
  timeTrackingGet(id) {
    return apiClient.get("/api/time_tracking/" + id);
  }
};
