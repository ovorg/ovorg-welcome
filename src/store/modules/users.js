import Api from "@/services/ApiService.js";
import _ from "lodash";
import Vue from "vue";

export const state = {
  users: []
};

export const mutations = {
  SET_USERS(state, users) {
    state.users = users;
  },
  UPDATE_PRESENCE(state, user) {
    let users = state.users;
    let id = _.findIndex(users, function(o) {
      return o.id == user.id;
    });
    if (id >= 0) users[id].present = JSON.parse(user.present);
    Vue.set(state, "users", [...users]);
  },
  UPDATE_USER(state, newUser) {
    let users = state.users;
    let id = _.findIndex(users, function(o) {
      return o.id == newUser.id;
    });
    if (id >= 0) users[id] = newUser;
    else users.push(newUser);
    Vue.set(state, "users", [...users]);
  }
};

export const actions = {
  fetchUsers({ commit }) {
    Api.timeTracking().then(response => {
      let sorted = _.orderBy(
        response.data.data,
        [
          function(e) {
            return methods.sortUsersByPosition(e.position);
          },
          "last_name",
          "first_name"
        ],
        ["desc", "asc", "asc"]
      );
      commit("SET_USERS", sorted);
    });
  },
  updateUser({ commit }, newUser) {
    commit("UPDATE_PRESENCE", newUser);
    Api.timeTrackingGet(newUser.id).then(response => {
      commit("UPDATE_USER", response.data.data);
    });
  }
};

export const getters = {
  presentUsers: state => {
    return state.users.filter(user => user.present === true);
  },
  stabUsers: state => {
    return _.filter(state.users, function(e) {
      if (e.division && e.division.name == "OV-Stab") return true;
      else return false;
    });
  },
  fuehrungUsers: state => {
    let ufue = _.filter(state.users, function(e) {
      if (e.position && (e.position.id == 2 || e.position.id == 3)) return true;
      else return false;
    });
    ufue = _.groupBy(_.orderBy(ufue, ["division.name"]), "division.name");
    let zfue = _.filter(state.users, function(e) {
      if (e.position && (e.position.id == 4 || e.position.id == 6)) return true;
      else return false;
    });
    return Object.values(_.merge({ Zugtrupp: zfue }, ufue));
  },
  otherUsers: state => {
    return _.filter(state.users, function(user) {
      if (user.division && user.division.name == "OV-Stab") {
        return false;
      }

      if (
        user.position &&
        (user.position.id == 2 ||
          user.position.id == 3 ||
          user.position.id == 4 ||
          user.position.id == 6)
      ) {
        return false;
      }

      return user.present;
    });
  },
  staerke: (state, getters) => {
    let zfue = _.filter(state.users, function(e) {
      if (e.present && e.position && e.position.id == 6) return true;
      else return false;
    }).length;
    let ufue = _.filter(state.users, function(e) {
      if (
        e.present &&
        e.position &&
        (e.position.id == 2 || e.position.id == 3 || e.position.id == 4)
      )
        return true;
      else return false;
    }).length;
    let he = getters.presentUsers.length - zfue - ufue;
    return (
      zfue +
      "/" +
      ufue +
      "/" +
      he +
      "/ <u>" +
      getters.presentUsers.length +
      "</u>"
    );
  }
};

const methods = {
  sortUsersByPosition(position) {
    if (!position) {
      return 0;
    }

    switch (position.id) {
      case 15:
        return 12;
      case 14:
        return 11;
      case 9:
        return 10;
      case 12:
        return 9;
      case 10:
        return 8;
      case 8:
        return 7;
      case 20:
        return 6;
      case 21:
        return 5;
      case 6:
        return 4;
      case 4:
        return 3;
      case 3:
        return 2;
      case 2:
        return 1;
      default:
        return 0;
    }
  }
};
