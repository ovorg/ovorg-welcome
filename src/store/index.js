import Vue from "vue";
import Vuex from "vuex";
import * as auth from "./modules/auth";
import * as users from "./modules/users";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    users
  },
  strict: debug
});
